# Welcome #

This project is a simple example of how a remote system would integrate the ASA24 Survey into an exiting application. It has been written in Java.

### Integration ###

Integration documentation is in the project under the /doc folder

### Contribution guidelines ###

Please report any issues on the issue tracker associated to this project, click on the Issues link in the left hand menu.

### Contacts ###

Please send questions to you Westat contact