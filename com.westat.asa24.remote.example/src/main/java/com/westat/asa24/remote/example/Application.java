/*
 * Copyright (c) 2016, Westat, Inc. All rights reserved. Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met: 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
 * following disclaimer. 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the distribution. 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without specific prior written permission. THIS SOFTWARE IS PROVIDED BY THE
 * COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.westat.asa24.remote.example;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.options;
import static spark.Spark.port;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import spark.Request;
import spark.Response;
import spark.Spark;

/**
 * An example of creating a JSON web token and redirecting the browser to the ASA24 site. This example uses the io.jsonwebtoken library for creating and
 * validating JWTs. This example uses the Spark framework for interacting with service endpoints.
 * 
 * @Westat
 */
public class Application {

   /** Secret used to sign the JSON Web Token. */
   private static final String SECRET    = "MmEwY2FmNmEtMTQ3My00YWE1LWIxNTItYzIwOTk2ODhkYzM2";

   /** Unique identifier for the configured study. This must be obtained from the ASA24 Researcher site. */
   private static final UUID   STUDY_ID  = UUID.fromString("BB045187-3EC0-412F-85BE-9945C0FD3538");

   /** The url to which the browser should be redirected. Replace this dummy URL with the ASA24 URL for remote system access. */
   private static final String URL_ASA24 = "http://localhost:9001/index.html";
   
   /** The url to which the generated token should be validated against before redirecting. Replace this dummy URL with the web service url for token validation. */
   private static final String URL_VALIDATE_TOKEN = "http://localhost:8090/api/account/remote/";

   /**
    * The main method.
    *
    * @param args the arguments
    */
   public static void main(String[] args) {

      // set the local port for running the example
      port(3000);

      Spark.staticFileLocation("/public");

      // Enable CORS
      Application.enableCORS("*", "GET, POST, OPTIONS", "origin, content-type, accept");

      // Define the route to create a token
      get("/asa24/", "application/x-www-form-urlencoded", (Request request, Response response) -> {
            // Quirk of Spark framework. Form fields are accessed through
            // queryParams and must be done so prior to reading request.body()
            String username = request.queryParams("username");
            String language = request.queryParams("language");

            // create the token
            String jwt = createJSONWebToken(language,  "http://localhost:3000/complete.html", "respondent", 
                  username);

            // Validate the json web token against a Westat endpoint before redirecting.
           HttpClient client = HttpClientBuilder.create().build();
           HttpPost post = new HttpPost(URL_VALIDATE_TOKEN);
           post.addHeader("content-type", "application/json");
           // Add the token to be validated to the body of the request.
           post.setEntity(new StringEntity("{ \"token\": \"" + jwt + "\"}"));

           HttpResponse validateResponse = client.execute(post);
           BufferedReader rd = new BufferedReader(new InputStreamReader(validateResponse.getEntity().getContent()));
           StringBuilder builder = new StringBuilder();
           String line = "";
           while ((line = rd.readLine()) != null) {
               builder.append(line);
           }

           // Convert the validation response in to a map and check the isValid property.
           HashMap<String,Object> result = (new ObjectMapper()).readValue(builder.toString(), new TypeReference<HashMap<String,Object>>() {});

           Boolean isValid = (Boolean)result.get("isValid");

           // Only redirect to the ASA24 survey if the token is valid.
           if (isValid) {
               // Redirect with the token in the query string.
               response.status(302);
               response.redirect(Application.URL_ASA24 + "?token=" + jwt);

               return null;
           }

           // If the token is not valid return an error and the response object.
           response.status(400);
           return (new ObjectMapper()).writeValueAsString(result);
        });
   }

   /**
    * Creates the JSON web token.
    *
    * @param language the language of the respondent, MUST be one of [eng | spa | fra]
    * @param redirect the the URL to which ASA24 will redirect on completion of the survey
    * @param role the role, MUST be one of [ respondent | researcher| system] 
    * @param username the user name, this should be an identifier which does not carry any user identifiable information
    * @return the string
    */
   private static String createJSONWebToken(String language, String redirect, String role, String username ) {

      // Create the claims required by ASA24
      HashMap<String, Object> claimsMap = new HashMap<>();
      claimsMap.put("language", language);
      claimsMap.put("redirect", redirect);
      claimsMap.put("role", role);
      claimsMap.put("study", Application.STUDY_ID);
      claimsMap.put("user", username);

      // add any specific claims required by your application
      // ....

      // Build the JWT
      String jwt = Jwts.builder()
            .signWith(SignatureAlgorithm.HS512, Application.SECRET)
            // replace with your organization name
            .setIssuer("Your_Organization_Name")
            .setSubject("users/" + username)
            // Must set claims from map before using setExpiration or exp will be overwritten.
            .setClaims(claimsMap)
            // suggested expiration is 24 hours
            .setExpiration(new Date(System.currentTimeMillis() + 3600000 * 24))
            .compact();

      return jwt;
   }

   /**
    * Enable CORS. This method is an initialization method and should be called once. See
    * <a href="https://sparktutorials.github.io/2016/05/01/cors.html">Spark CORS</a>
    *
    * @param origin the origin
    * @param methods the methods
    * @param headers the headers
    */
   private static void enableCORS(final String origin, final String methods, final String headers) {

      options("/*", (request, response) -> {

         String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
         if (accessControlRequestHeaders != null) {
            response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
         }

         String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
         if (accessControlRequestMethod != null) {
            response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
         }

         return "OK";
      });

      before((request, response) -> {
         response.header("Access-Control-Allow-Origin", origin);
         response.header("Access-Control-Request-Method", methods);
         response.header("Access-Control-Allow-Headers", headers);
         // Note: this may or may not be necessary in your particular application
         response.type("application/json");
      });
   }
}
